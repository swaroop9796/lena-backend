============
Installation
============

--------------
Pre-Requisites
--------------
* `Python3.7`_


Installing the Project
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    1. Run 'python3 -m venv ~/lena_env'

    2. Run 'source ~/lena-env/bin/activate'

    3. Run 'sudo apt-get install wkhtmltopdf' once during initial setup

    4. Run 'pip3 install -r dependencies.txt' to install dependency packages

    5. In configuration.json, change the configurations according to your local.

    6. In 'src_code/js_secure_string' location, open terminal and run 'npm install' once during initial setup
        (After nmp install run 'node secure_string -e <string>' and replace encrypted string in configuration.json)

    7. From 'device-data-collector/' directory run 'python3 src_code/document_service/generate_PDF.py'
