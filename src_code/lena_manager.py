import logging

from src_code.utils import global_variables
from src_code.api.flask.main import start_flask_server
from src_code.utils.basic import get_exception_details
from src_code.services.mongo_handler import MongoHandler

LOG = logging.getLogger(__name__)

class LenaManager(object):
    def __init__(self):
        pass

    def start_lena_services_and_resources(self):
        try:
            start_flask_server()

            global_variables.mongo_handler = MongoHandler()



        except Exception as e:
            LOG.error(get_exception_details(e))
            raise