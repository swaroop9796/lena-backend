import sys

class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

def get_exception_details(_):  # Not using the incoming parameter, Exception object
    exc_type, exc_obj, tb = sys.exc_info()
    line_no = tb.tb_lineno
    return 'Exception at line:{},{}:{}'.format(line_no, exc_type, exc_obj)