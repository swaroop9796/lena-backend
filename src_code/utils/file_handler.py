import logging
import yaml

from src_code.utils.basic import get_exception_details

LOG = logging.getLogger(__name__)

def read_data_from_yaml(file):
    try:
        with open(file, 'r') as f:
            data = yaml.load(f, yaml.FullLoader)
        return data
    except Exception as e:
        LOG.error(get_exception_details(e))
        return None

def write_data_to_yaml(file, data):
    try:
        with open(file, 'w') as f:
            yaml.dump(data, f, default_flow_style=False)
    except Exception as e:
        LOG.error(get_exception_details(e))
        return None