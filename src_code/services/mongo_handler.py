import logging

from pymongo import MongoClient
from constants.file_path import *
from src_code.utils.basic import Singleton, get_exception_details
from src_code.utils.file_handler import read_data_from_yaml

LOG = logging.getLogger(__name__)

class MongoHandler(metaclass=Singleton):   # As Singleton is used as metaclass, all instances will be same as first instantiation
    def __init__(self):
        db_config = read_data_from_yaml(DB_YAML_PATH)
        self.mongo_config = db_config["mongodb"]
        self.client = MongoClient(self.mongo_config['host'], self.mongo_config['port'])
        db_list = self.client.list_database_names()
        if self.mongo_config["db_name"] in db_list:
            self.db = self.client[self.mongo_config["db_name"]]
        else:
            self.create_db_and_collections()

    def create_db_and_collections(self):
        try:
            self.db = self.client[self.mongo_config["db_name"]]
            self.db.abc.insert_one({"a":"a"})
        except Exception as e:
            LOG.error(get_exception_details(e))
