from src_code.utils.file_handler import *

def modify_event_configuration(data, action):
    try:
        events_config = read_data_from_yaml('configuration/events.yaml')
        if action == "fire_all_events":
            events_config["fire_all_events"] = data
        if action == "add":
            events_config["fire_all_events"] = False
            events_config["subscribed_events"] = data
        elif action == "remove":
            events_config["subscribed_events"] = [event for event in events_config["subscribed_events"] if event not in data]
        write_data_to_yaml('configuration/events.yaml', events_config)
    except Exception as e:
        LOG.error(get_exception_details(e))
        raise