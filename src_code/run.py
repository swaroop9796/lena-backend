import logging.config
import sys
import yaml

from os import getcwd

sys.path.append(getcwd())
from src_code.utils.basic import get_exception_details
from src_code.lena_manager import LenaManager

LOG = logging.getLogger(__name__)

def log_startup_info():
    """Log info about the current environment."""
    LOG.info('Starting custom program')

def setup_logging(logging_conf):
    logging.config.dictConfig(logging_conf)
    return

def main():
    try:
        with open('configuration/logging.yaml', 'r') as f:
            logging_config = yaml.load(f, yaml.FullLoader)
        setup_logging(logging_config["logging"])
        log_startup_info()
        lena_manager = LenaManager()
        lena_manager.start_lena_services_and_resources()
    except Exception as e:
        LOG.error(get_exception_details(e))

if __name__ == "__main__":
    main()