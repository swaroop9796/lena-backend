



















#------------------------------------------------------------------------------------------------------------------------------------

import json
import shutil
import threading
import time
from glob import glob

from flask import Flask, request, abort, jsonify
from flask_cors import CORS
import hashlib
import os, signal
import datetime
import yaml

from subprocess import check_output
from os.path import expanduser

from lena.api.service.Mode_Configurer import ResourceModeConfigurer
from lena.api.service.Thermal_Resource_Service import ThermalResourceService
from lena.constants import *
from lena import global_vars
from lena.operations.JOG.jog_service import JogService
from lena.operations.MDI.MDI_service import MDI_Service
from lena.basic import SenseOSConfig
from lena.resources.machine import EventNetDisconnected, EventNetConnected
from lena.settings import SENSEOS_CONFIG_PATH_DEFAULT
from lena.storage.persistence import Persistence
from lena.resources.current_print import EventSelectFile, EventReplayGcode
from lena.utils import display_time
from lena.utils.lock import ReadWriteLock
from lena.utils.utilty import get_lines_count, read_line
import linuxcnc
from lena.resources.lcnc import Lcnc

app = Flask(__name__)
CORS(app)
app.debug = False
app.use_reloader = False

temp_base = os.path.expanduser(os.getcwd() + "/Gcode")
mdi_cmd_history_yaml = os.getcwd() + "/.runtime/var/lena/persistence/Gcode.yaml"

@app.route("/")
def test():
    return "Event Fired"


@app.route("/changeModeToManual", methods=['GET'])
def changeModeToManual():
    try:
        c = linuxcnc.command()
        s = linuxcnc.stat()
        s.poll()
        """
        MODES
        MODE_AUTO = 2
        MODE_MANUAL = 1
        MODE_MDI = 3

        STATES

        LCNC_IDLE = 1
        LCNC_BUSY = 2
        LCNC_RCS_ERROR = 3
        """
        response = ""
        # print
        print "s.estop", s.estop
        print "s.enabled", s.enabled
        if s.estop == 0:
            if s.enabled:
                if s.interp_state == Lcnc.LCNC_IDLE:
                    if s.task_mode != linuxcnc.MODE_MANUAL:
                        c.mode(linuxcnc.MODE_MANUAL)
                        c.wait_complete()
                        response = "Machine is Ready"
                    else:
                        response = "Machine is Ready and in JOG Mode"
                else:
                    response = "Machine is not in Idle mode, Bring Machine in Idle state"
            else:
                response = "Machine is not ON"
        else:
            response = "Machine is not in eclear State"

        return jsonify({"RESPONSE": response, "MACHINE_INTERP_STATE": s.interp_state, "TASK_MODE": s.task_mode})

    except Exception as e:
        app.logger.debug('Exception : %s', e)
        return jsonify({"Exception": e})


@app.route("/execute_mdi_command", methods=['POST'])
def execute_mdi_command():
    mdi_command = str(json.loads(request.data)["mdi_command"])
    command_data = {
        "command": mdi_command,
        "timestamp": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        "description": "",
        "status": ""
    }
    mdi_service = MDI_Service()
    mdi_status = mdi_service.get_mdi_status()

    if mdi_status == "MDI MODE":
        lcnc = global_vars.lcnc_obj
        mdi_service.execute_mdi_command(lcnc, mdi_command)
        while mdi_service.get_execution_status() != linuxcnc.RCS_DONE:
            pass
        command_data["description"] = "Command Executed Successfully"
        command_data["status"] = "Success"
    else:
        command_data["description"] = "Not in MDI Mode : " + mdi_status
        command_data["status"] = "ERROR"

    if os.path.exists(mdi_cmd_history_yaml):
        with open(mdi_cmd_history_yaml, 'r') as file:
            mdi_command_history = yaml.load(file, Loader=yaml.FullLoader)
            mdi_command_history["MDI_COMMAND"].append(command_data)
    else:
        mdi_command_history = {"MDI_COMMAND":[]}
        mdi_command_history["MDI_COMMAND"].append(command_data)
    with open(mdi_cmd_history_yaml, 'w') as file:
        documents = yaml.dump(mdi_command_history, file)

    return jsonify(command_data)


@app.route("/execute_jog", methods=['POST'])
def execute_jog():
    data = json.loads(request.data)
    direction = data['direction']
    axis = data['axis']
    speed = data['speed']
    mode = data['mode']
    step = data['step']
    response = {
        "status": "",
        "description": ""
    }
    if step:
        step = float(step)
    jog_service = JogService()
    if jog_service.is_machine_on():
        if jog_service.check_pending_executions():
            velocity, axis = jog_service.compute_velocity(axis, speed)
            jog_mode = jog_service.get_joint_jog_mode()
            if direction == "increment":
                jog_service.execute_jog(int(mode), jog_mode, axis, velocity, step)
            else:
                jog_service.execute_jog(int(mode), jog_mode, axis, -velocity, step)
            while jog_service.get_execution_status() != linuxcnc.RCS_DONE:
                pass
            response["status"] = "Success"
            response["description"] = "Jog Executed Successfully"
        else:
            response["status"] = "Error"
            response["description"] = "Cannot Execute Jog, MDI Command Execution is in Progress"
    else:
        response["status"] = "Error"
        response["description"] = "Cannot Execute Jog, Machine is in ESTOP or NOT ENABLED State"
    return jsonify(response)

@app.route("/stop_jog", methods=['POST'])
def stop_jog():
    response = {
        "status": "",
        "description": ""
    }
    jog_service = JogService()
    jog_mode = jog_service.get_joint_jog_mode()
    jog_service.stop_jog(jog_mode)
    response["status"] = "Success"
    response["description"] = "Jog Stopped Successfully"
    return jsonify(response)


@app.route("/mdi_command_history", methods=['GET'])
def get_mdi_command_history():
    with open(mdi_cmd_history_yaml) as file:
        commands = yaml.load(file, Loader=yaml.FullLoader)
        return jsonify(commands)


@app.route("/execute_arbitary_gcode", methods=['POST'])
def execute_arbitary_gcode_line():
    arbitary_line_number = json.loads(request.data)["line_number"]
    EventReplayGcode.fire(source=__name__, data={"line_number": int(arbitary_line_number)})
    return jsonify({"status": 200})


@app.route("/search_gcode_line", methods=['POST'])
def search_gcode_line():
    line_number = json.loads(request.data)["line_number"]
    gcode_lines = get_gcode_lines(int(line_number) - 1, 100, 'scroll_down');
    return jsonify({"status": 200, "data": gcode_lines})

@app.route("/set_feedrate", methods=['POST'])
def set_feedrate():
    try:
        stats_obj = linuxcnc.stat()
        stats_obj.poll()
        command_obj = linuxcnc.command()
        set_feedrate = json.loads(request.data)["feedrate"]
        command_obj.feedrate(set_feedrate)
        command_obj.wait_complete()
        stats_obj.poll()
        return jsonify({"updated_feedrate":stats_obj.feedrate})
    except linuxcnc.error, detail:
        app.logger.debug('Exception occurred in set_feedrate: %s', detail)
        return jsonify({"exception": detail})


@app.route("/get_gcode_lines", methods=['GET'])
def get_gcode_lines_handler():
    number_of_buffer_lines = 200
    line_number = int(request.args.get('line_number', type=str))
    action = request.args.get('type', type=str)
    try:
        number_of_buffer_lines = int(request.args.get('number_of_buffer_lines', type=str))
        print line_number, number_of_buffer_lines, action
    except Exception as e:
        print e.message()
    gcode_lines = get_gcode_lines(line_number, number_of_buffer_lines, action)
    return jsonify({"status": 200, "data": gcode_lines})


@app.route("/validate_gcode", methods=['POST'])
def validate_gcode():
    gcode_data = json.loads(request.data)
    result, message = validate_gcode_data(gcode_data["header"], HEADER_GOLDEN_LIST)
    response = {"is_valid": result, "error_code": message}
    return jsonify(response)


@app.route("/history_of_alarms", methods=['GET'])
def history_of_alarms():
    try:
        alarms = {}
        machine_data = global_vars.machine_persistence_obj.read_yaml()

        for key, value in machine_data.items():
            if "alarm" in key:
                alarms[key] = value
        return jsonify(alarms)
    except Exception as e:
        app.logger.debug('Exception occurred in history_of_alarms: %s', e)
        alarms_exception = "Exception occurred :" + str(e)
        return jsonify({"alarms": alarms_exception})


@app.route("/get_stats_data", methods=['GET'])
def get_stats_data():
    try:
        stats = global_vars.machine_persistence_obj.read_yaml()
        stats["lifetime"]=display_time(stats["lifetime"])
        return jsonify(stats)
    except Exception as e:
        app.logger.debug('Exception occurred in get_stats_data: %s', e)
        stats_exception = "Exception occurred :" + str(e)
        return jsonify({"stats": stats_exception})


@app.route("/get_machine_config", methods=['GET'])
def get_machine_config():
    try:
        sos_confg = SenseOSConfig.get()
        spool_bay_sos_config = sos_confg["resources"]["SpoolBay"]
        file_locks = {}
        SpoolBay_resourse_used_file_path = os.getcwd() + "/" + SENSEOS_CONFIG_PATH_DEFAULT + "SpoolBay.yaml"
        file_locks[SpoolBay_resourse_used_file_path] = ReadWriteLock()
        spool_bay_persistence_obj = Persistence(SpoolBay_resourse_used_file_path, "",
                                                file_locks[SpoolBay_resourse_used_file_path])
        spool_bay_data = spool_bay_persistence_obj.read_yaml()
        get_machine_config = {}
        for key, value in spool_bay_sos_config.items():
            get_machine_config[value] = spool_bay_data[value]["commercial_name"]

        print(get_machine_config)
        return jsonify(get_machine_config)
    except Exception as e:
        app.logger.debug('Exception Get Machine Congif: %s', e)
        get_machine_config_exception = "Exception Occurred " + str(e)
        return get_machine_config_exception

@app.route("/get_network_status", methods=['GET'])
def get_network_status():
    try:
        hostname = "google.com"
        response = os.system("ping -c 1 " + hostname + ">/dev/null 2>&1")
        temp_data = {}
        machine_data = global_vars.machine_persistence_obj.read_yaml()

        temp_data["category"] = "alarm_network_error"
        temp_data["time_stamp"] = datetime.datetime.now()
        temp_data["ping_response"] = str(response)
        if response != 0:
            ping_status = "Network Error"
            if machine_data["is_network_connected"]:
                machine_data["is_network_connected"] = False
                EventNetDisconnected.fire(source=__name__, data={})
                temp_data["type"] = "ERROR"
                temp_data["description"] = str(ping_status)
                machine_data["alarm_network_error"].append(temp_data)
        else:
            ping_status = "Network Active"
            if not machine_data["is_network_connected"]:
                machine_data["is_network_connected"] = True
                EventNetConnected.fire(source=__name__, data={})
                temp_data["type"] = "WORKING"
                temp_data["description"] = str(ping_status)
                machine_data["alarm_network_error"].append(temp_data)

        global_vars.machine_persistence_obj.write_yaml(machine_data)
        return jsonify({"ping_status": ping_status})
    except Exception as e:
        app.logger.debug('Network Issue: %s', e)
        network_exception = "Exception Occurred " + str(e)
        return network_exception


@app.route("/get_material_and_build_time_from_gcode", methods=['GET'])
def get_material_and_build_time_from_gcode():
    try:
        response = {}
        header = []
        tail = []
        hours = minutes = 0
        if os.path.isfile(loaded_gcode_file):
            global_vars.total_number_of_lines_in_gcode_file = total_lines = get_lines_count(loaded_gcode_file)
            with open(loaded_gcode_file) as file:
                for line in (file.readlines()[:20]):
                    if "printMaterial" in line:
                        material_command = line
                        break
            with open(loaded_gcode_file) as file:
                for line in (file.readlines()[-7:]):
                    if "Build time" in line:
                        build_time_comand = line
                        break
            if material_command and build_time_comand:
                material = material_command.split(",")[1].strip()
                simplify3D_build_time = build_time_comand.split(":")[1].strip()
                if "hour" in simplify3D_build_time:  # TODO this logic is based on the assumption that build time will always be inthis format "X hour Y minutes"
                    hours = simplify3D_build_time.split()[0]
                if "minutes" in simplify3D_build_time:
                    minutes = simplify3D_build_time.split()[-2]
                simplify3D_build_time_in_seconds = int(hours) * 3600 + int(minutes) * 60
                approx_build_time_in_seconds = simplify3D_build_time_in_seconds + simplify3D_build_time_in_seconds * 3 / 20
                approx_build_time_in_hours_and_mins_and_seconds = str(datetime.timedelta(seconds=approx_build_time_in_seconds))
                response = {"material": material, "build_time": approx_build_time_in_hours_and_mins_and_seconds}
            else:
                return jsonify({"material": "", "build_time": ""})
        else:
            response = {"error": "File not loaded"}
        return jsonify(response)
    except:
        return jsonify({"material":"", "build_time": ""})


# resumable.js uses a GET request to check if it uploaded the file already.
# NOTE: your validation here needs to match whatever you do in the POST (otherwise it will NEVER find the files)
@app.route("/resumable_upload", methods=['GET'])
def resumable():
    resumableIdentfier = request.args.get('resumableIdentifier', type=str)
    resumableFilename = request.args.get('resumableFilename', type=str)
    resumableChunkNumber = request.args.get('resumableChunkNumber', type=int)

    if not resumableIdentfier or not resumableFilename or not resumableChunkNumber:
        # Parameters are missing or invalid
        abort(500, 'Parameter error')

    # chunk folder path based on the parameters
    temp_dir = os.path.join(temp_base, resumableIdentfier)
    print "temp dir- ", temp_dir

    # chunk path based on the parameters
    chunk_file = os.path.join(temp_dir, get_chunk_name(resumableFilename, resumableChunkNumber))
    app.logger.debug('Getting chunk: %s', chunk_file)
    print 'Getting chunk: ', chunk_file

    if os.path.isfile(chunk_file):
        # Let resumable.js know this chunk already exists
        return 'OK'
    else:
        # Let resumable.js know this chunk does not exists and needs to be uploaded
        abort(404, 'Not found')


# if it didn't already upload, resumable.js sends the file here
@app.route("/resumable_upload", methods=['POST'])
def resumable_post():
    print request.form.get('token', type=int)
    resumableTotalChunks = request.form.get('resumableTotalChunks', type=int)
    resumableChunkNumber = request.form.get('resumableChunkNumber', default=1, type=int)
    resumableFilename = request.form.get('resumableFilename', default='error', type=str)
    resumableIdentfier = request.form.get('resumableIdentifier', default='error', type=str)
    src_hash_string = request.form.get('SHA256HASH', default='error', type=str)

    file_ = "/Gcode" + resumableFilename
    # get the chunk data
    chunk_data = request.files['file']

    # make our temp directory
    temp_dir = os.path.join(temp_base, resumableIdentfier)
    if not os.path.isdir(temp_dir):
        os.makedirs(temp_dir, 0o777)

    # save the chunk data
    chunk_name = get_chunk_name(resumableFilename, resumableChunkNumber)
    chunk_file = os.path.join(temp_dir, chunk_name)
    chunk_data.save(chunk_file)
    app.logger.debug('Saved chunk: %s', chunk_file)

    if resumableTotalChunks == resumableChunkNumber:
        tail = []
        with open(chunk_file) as file:
            for line in (file.readlines()[-7:]):
                tail.append(line)
        result, message = validate_gcode_data(tail, FOOTER_GOLDER_LIST)
    if not result:
        try:
            response = jsonify({"is_valid": result, "error_code": message})
            shutil.rmtree(temp_dir)
            return "invalid footer", 500
        except Exception as e:
            app.logger.debug('Exception: %s', e)

    # check if the upload is complete
    chunk_paths = [os.path.join(temp_dir, get_chunk_name(resumableFilename, x)) for x in
                   range(1, resumableTotalChunks + 1)]
    upload_complete = all([os.path.exists(p) for p in chunk_paths])

    # combine all the chunks to create the final file
    if upload_complete:
        target_file_name = os.path.join(temp_base, resumableFilename)
        app.logger.debug('target_file_name: %s', target_file_name)
        with open(target_file_name, "ab") as target_file:
            for p in chunk_paths:
                stored_chunk_file_name = p
                stored_chunk_file = open(stored_chunk_file_name, 'rb')
                target_file.write(stored_chunk_file.read())
                stored_chunk_file.close()
                os.unlink(stored_chunk_file_name)
        target_file.close()
        dst_hash_string = get_hash_of_file(target_file_name)
        app.logger.debug('SRC----------: %s', src_hash_string)
        app.logger.debug('DST----------: %s', dst_hash_string)
        print 'SRC----------: %s', src_hash_string
        print 'DST----------: %s', dst_hash_string
        if src_hash_string != dst_hash_string:
            return jsonify(
                {"is_valid": False, "error_code": "Data corrupted in the file received. Please upload again"})
        os.rmdir(temp_dir)
        app.logger.debug('File saved to: %s', target_file_name)
        global loaded_gcode_file
        loaded_gcode_file = target_file_name
        global total_num_of_lines
        total_num_of_lines = get_lines_count(loaded_gcode_file)
        print "---", target_file_name
        EventSelectFile.fire(source=__name__, data={"file_name": target_file_name})
    return 'OK'


def get_gcode_lines(line_number, number_of_buffer_lines, action):
    gcode_lines = list()
    if action == "search":
        if line_number - number_of_buffer_lines < 0:
            starting_line = 1
        else:
            starting_line = line_number - number_of_buffer_lines
        if line_number + number_of_buffer_lines > total_num_of_lines:
            ending_line = total_num_of_lines + 1
        else:
            ending_line = line_number + number_of_buffer_lines + 1
    elif action == "scroll_up":
        ending_line = line_number
        if line_number - number_of_buffer_lines < 0:
            starting_line = 1
        else:
            starting_line = line_number - number_of_buffer_lines
    elif action == "scroll_down":
        starting_line = line_number + 1
        if line_number + number_of_buffer_lines > total_num_of_lines:
            ending_line = total_num_of_lines + 1
        else:
            ending_line = line_number + number_of_buffer_lines + 1
    for line_no in range(starting_line, ending_line):
        result = read_line(loaded_gcode_file, line_no)
        line_data = {"line": result.rstrip(), "line_number": line_no}
        gcode_lines.append(line_data)
    return gcode_lines


def validate_footer(footer_list):
    pass
    # result, message = validate_gcode_data(gcode_data["header"], HEADER_GOLDEN_LIST)


def get_hash_of_file(filename):
    h = hashlib.sha256()
    b = bytearray(128 * 1024)
    mv = memoryview(b)
    with open(filename, 'rb', buffering=0) as f:
        for n in iter(lambda: f.readinto(mv), 0):
            h.update(mv[:n])
    return h.hexdigest()


def get_chunk_name(uploaded_filename, chunk_number):
    return uploaded_filename + "_part_%03d" % chunk_number


def stop_flask_server():
    os.kill(os.getpid(), signal.SIGINT)


def start_flask_server():
    threading.Thread(target=app.run, kwargs={'port': '5000', 'host': '0.0.0.0'}).start()


def validate_gcode_data(gcode_data, golden_list):
    try:
        settings = {}
        error_code = []
        sense_os_config_path = os.getcwd() + "/lena_config/senseos_config.yaml"
        spoolbay_config_path = os.getcwd() + "/lena_config/SpoolBay.yaml"
        fdm_tool_config_path = os.getcwd() + "/lena_config/FdmTool.yaml"

        with open(sense_os_config_path, 'r') as file:
            data = yaml.load(file, Loader=yaml.FullLoader)
            machine_name = data["machine_name"]
        with open(spoolbay_config_path, 'r') as file:
            data = yaml.load(file, Loader=yaml.FullLoader)
            material_loaded = data["AHA_SPOOL0"]["material_name"]

        for index, line in enumerate(gcode_data):
            if line:
                if line[0] == ';':
                    if index == 0 and len(gcode_data) > 190:
                        if SIMPLIFY3D not in line:
                            error_code.append(NOT_FROM_SIMPLIFY3D)
                            return False, NOT_FROM_SIMPLIFY3D
                        if VERSION not in line:
                            error_code.append(WRONG_VERSION)
                    if ',' in line:
                        tmp_line_list = ((line.strip(';')).strip()).split(',')
                        if PRINT_MATERIAL in line:
                            printMaterial = tmp_line_list[1]
                            if printMaterial != material_loaded:
                                error_code.append(MATERIAL_NOT_MATCHED)
                        if PROFILE_NAME in line:
                            profileName = tmp_line_list[1]
                            if profileName != machine_name:
                                error_code.append(PROFILE_NOT_MATCHED)
                        if EXTRUDER_NAME in line:
                            tool_required = tmp_line_list[1]
                            with open(fdm_tool_config_path, 'r') as file:
                                data = yaml.load(file, Loader=yaml.FullLoader)
                            if tool_required not in data.keys():
                                error_code.append(TOOL_NOT_MATCHED)
                        line_list = []
                        for i in tmp_line_list:
                            line_list.append(i.strip())
                    else:
                        tmp_line_list = ((line.strip(';')).strip()).split(':')
                        line_list = []
                        for i in tmp_line_list:
                            line_list.append(i.strip())
                    if line_list[0] not in settings.keys():
                        settings[line_list.pop(0)] = line_list
                        # Here line_list.pop(0) gives us the name of the parameter, and thereafter the left part gives us the value corresponding to that particular parameter.
                        # Hence, Here we can see that lock list is being assigned as the value of the parameter in the settings dict.
                        # This list can have any number of values from 1 to n.
                    else:
                        error_code.append("'" + line_list[0] + "'" + DUPLICATE_TAGS)
                else:
                    # This can never happen, as the line is given to this method only after checking that it begins with ';'
                    # This is there just for the worst/ unpredicted scenarios.
                    error_code.append(EXPECT_COMMENT + str(index + 1))

        for i in golden_list:  # Golden list is the list of all the tags that should be there in any Gcode file.
            if i not in (settings).keys():  # Settings is lock dict whose keys are all the tags that are there in the selected Gcode file.
                error_code.append("'" + i + "'" + TAG_NOT_FOUND)
        if error_code:
            settings.clear()
            return False, error_code
        settings.clear()
        return True, GCODE_VALID
    except Exception as e:
        app.logger.debug('Exception: %s', e)

@app.route("/get_version", methods=['GET'])
def get_version():
    version_file = os.getcwd() + "/versions.txt"
    with open(version_file) as file:
        version = file.read()
    return version

@app.route("/load_file", methods=['POST'])
def load_file():
    data = json.loads(request.data)
    filename = data["filename"]
    home = expanduser("~")
    output = check_output(["lsblk", "-o", "MOUNTPOINT"])
    gcode_src_folder = [path for path in output.split() if path.count('/')>1]
    gcode_src_folder = [path.replace(path, path+"/Gcodes/"+filename) for path in gcode_src_folder]
    gcode_dst_folder = home + "/Gcodes/"
    gcode_dst_path = gcode_dst_folder + filename.split('/')[-1]
    final_src_path = ""
    result = None
    for index, src_path in enumerate(gcode_src_folder):
        if os.path.isfile(src_path):
            header = []
            with open(src_path) as file:
                for line in (file.readlines()[:193]):
                    header.append(line)
            header_result, header_message = validate_gcode_data(header, HEADER_GOLDEN_LIST)
            tail = []
            with open(src_path) as file:
                for line in (file.readlines()[-7:]):
                    tail.append(line)
            footer_result, footer_message = validate_gcode_data(tail, FOOTER_GOLDER_LIST)
            if not header_result or not footer_result:
                result = {"is_valid": False, "description": header_message+footer_message}
            final_src_path = src_path
            break
        else:
            if index < len(gcode_src_folder)-1:
                continue
            else:
                result = {"is_valid": False, "description": "Gcode not found"}
    if not result:
        if os.path.isdir(gcode_dst_folder):
            try:
                shutil.copyfile(final_src_path, gcode_dst_path)
                result = {"is_valid": True, "description": "File loaded"}
            except Exception as e:
                result = {"is_valid": False, "description": e}
        else:
            try:
                os.makedirs(gcode_dst_folder)
                shutil.copyfile(final_src_path, gcode_dst_path)
                result = {"is_valid": True, "description": "File loaded"}
            except Exception as e:
                result = {"is_valid": False, "description": e}
    if result["is_valid"]:
        global loaded_gcode_file
        loaded_gcode_file = gcode_dst_path
        global total_num_of_lines
        total_num_of_lines = get_lines_count(loaded_gcode_file)
        EventSelectFile.fire(source=__name__, data={"file_name": gcode_dst_path})
    return jsonify(result)


@app.route("/switch_mode", methods=['POST'])
def mode_configuration():
    data = json.loads(request.data)
    try:
        mode_configurer = ResourceModeConfigurer()
        mode_configurer.switch_mode(data['mode'])
        response = {"status": "success", "description": "Mode Switched to "+data['mode']}
    except Exception as e:
        print e
        response = {"status": "success", "description": "Unable to switch mode to "+data['mode']}
    return jsonify(response)

@app.route("/set_target_temperature", methods=['POST'])
def set_target_temperature():
    data = json.loads(request.data)
    try:
        thermal_service = ThermalResourceService()
        thermal_service.setTargetTemperature(data['id'], data['target_temperature'], data['resource'])
    except Exception as e:
        print e
    return jsonify({'status' : 'success', 'description': 'Setting the temperature.'})
