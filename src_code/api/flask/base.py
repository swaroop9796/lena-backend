import logging
import threading

import yaml
from flask import Flask
from src_code.api.flask.events_api import events_api
from src_code.utils.basic import get_exception_details

LOG = logging.getLogger(__name__)

app = Flask(__name__)
app.register_blueprint(events_api)

def start_flask_server():
    try:
        with open('configuration/api.yaml', 'r') as f:
            api_config = yaml.load(f, yaml.FullLoader)
        flask_config = api_config["flask"]
        kwargs = {'port': flask_config['port'], 'host': flask_config['host']}
        threading.Thread(target=app.run, kwargs=kwargs).start()
    except Exception as e:
        LOG.error(get_exception_details(e))
        raise