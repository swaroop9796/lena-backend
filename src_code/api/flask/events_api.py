import json
import logging
from flask import Blueprint, request, jsonify

from src_code.services.events import *
from src_code.utils.basic import get_exception_details

events_api = Blueprint('events_api', __name__)
LOG = logging.getLogger(__name__)

@events_api.route("/subscribe_events", methods=['POST'])
def subscribe_events():
    try:
        event_list = json.loads(request.data)["events"]
        modify_event_configuration(event_list, "add")
        return jsonify({'status': "success", "description": "Events subscribed"})
    except Exception as e:
        LOG.error(get_exception_details(e))
        return jsonify({'status': "failure", "description": ""})

@events_api.route("/unsubscribe_events", methods=['POST'])
def unsubscribe_events():
    try:
        event_list = json.loads(request.data)["events"]
        modify_event_configuration(event_list, "remove")
        return jsonify({'status': "success", "description": "Events unsubscribed"})
    except Exception as e:
        LOG.error(get_exception_details(e))
        return jsonify({'status': "failure", "description": ""})

@events_api.route("/fire_all_events", methods=['POST'])
def fire_all_events():
    try:
        data = json.loads(request.data)["fire_all_events"]
        modify_event_configuration(data, "fire_all_events")
        return jsonify({'status': "success", "description": "Fire all events field modified"})
    except Exception as e:
        LOG.error(get_exception_details(e))
        return jsonify({'status': "failure", "description": ""})